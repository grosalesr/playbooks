# Ansible Automation

## Important things to remember

* Create an [inventory](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html) file.
* Roles and Playbooks could have their own variables.
* Always take into consideration [variable precedence](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_variables.html#variable-precedence-where-should-i-put-a-variable)
    * By default, variables are merged/flattened to the specific host before a play is run. Groups do not survive outside of inventory and host matching.
    * Ansible merges groups at the same parent/child level in ASCII order, and variables from the last group loaded overwrite variables from the previous groups.
    * See [How variables are merged](https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#how-variables-are-merged)
* Know the [special variables](https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html)
* [Ansible Releases & maintenance](https://docs.ansible.com/ansible/latest/reference_appendices/release_and_maintenance.html)

### Ansible Engine

* `-K`: Ask sudo user password
* `-k`: Ask SSH user password
* `-i`: Inventory file's full path
* `-u`: Remote user
    * only needed when the user on the targets is different from the user that is executing the playbook.

* `tags` are used to make playbooks flexible enough to execute or omit specific tasks in the playbook's role.
    * `--tags`: Only executes a given task or tasks(separated by comma) from a role
    * `--skip-tags`: Skips a given task or tasks(separated by comma) from a role

## Roles

* common
* development
* gnome (Fedora Workstation)
* plasma (Fedora KDE Plasma)

**Notice** some of them use `never` tag for some tasks. These tasks must be called explicitly. See [Special tags](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html#special-tags)

## Playbooks

> check playbooks directory

### Generic Play

Instead of modifying a playbook, `generic_play.yml` acts as a generic playbook (duh!) which has the following variables:

* `target`, pass target servers on runtime.
* `role`, pass the roles needed.

execution examples:

* **Install RPM Fusion & Multimedia packages**\
`ansible-playbook -K play.yml --extra-vars "role=common target=<TARGET>" --tags rpm_fusion,multimedia`

* **Execute Development role**\
`ansible-playbook -K play.yml --extra-vars "role=development target=<TARGET> development_user=<USER>"`

* Execute **only** the task with tag `update_os`\
`ansible-playbook -Kki inventory -u regular_user play.yml --extra-vars "target=all" --tags update_os`

* Execute everything **except** the task with tag `flathub`\
`ansible-playbook -Kki inventory -u regular_user play.yml --extra-vars "target=qa" --skip-tags flathub`

* Runs Fedora's `setup-Workstation` playbook on localhost and skip some tasks:\
`ansible-playbook Fedora/setup-Workstation.yml --extra-vars "target=localhost" --ask-become-pass --skip-tags hardware_acceleration,reboot`

### Fedora Setup

Configures a Fedora release to my needs.

* common role is always executed
* gnome, plasma & development roles are imported.
    * `never` tag is used for all imports.
    * each has a tag named the same as the role.
        * due to [tag inheritance](https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_tags.html#tag-inheritance-adding-tags-to-multiple-tasks), all tasks will be executed.

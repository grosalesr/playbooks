#!/bin/bash

# globals
cmd="kwriteconfig5"
config_file="kwinrc"
cmd_run="$cmd --file $config_file"

# set Meta key to Overview
$cmd_run \
  --group ModifierOnlyShortcuts \
  --key Meta "org.kde.krunner,/App,,toggleDisplay"

# 6 virtual desktops
$cmd_run \
  --group Desktops \
  --key Number 6

# reconfigure KWin
qdbus org.kde.KWin /KWin reconfigure

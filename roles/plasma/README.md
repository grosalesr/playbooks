# plasma

Configures Fedora KDE spin

> Kde Plasma is not simple to configure using CLI tools.
> This is Work in progress for Plasma 5. Not tested/updated to Plasma 6.

Some changes are done using `kwriteconfig5`, examples:
 * [gentoox postinstall](https://github.com/fatalhalt/gentoox/blob/master/postinstall.sh)

## Look & Feel
* Inter variable font
* Papirus icon theme (System & Libreoffice)

Changes **must** be done manually using "System Settings" & Libreoffice.

Role Variables
--------------

None at the moment

License
-------

GPLv3 or later

Author Information
------------------

Gerardo Rosales
